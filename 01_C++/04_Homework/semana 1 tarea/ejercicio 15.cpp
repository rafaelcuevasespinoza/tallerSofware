
//Algorithm for calculate a slope and the midpoint of a line 

/*
@rafaelcuevasespinoza@gmail.com
@Date 4/12/2021

*/

/***********************************************
 *  												INCLUDE
 ***********************************************/

//iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii



#include <iostream>

using namespace std;


/***********************************************
 *  												MAIN
 ***********************************************/


//Using the function int main


int main(){
	
	//Declaration and Initialization of the variables
	
	float X1 = 0.0; //Point 1 of the abcissa
	float Y1 = 0.0; //Point 1 of the ordinate
	float X2 = 0.0; //Point 2 of the abcissa
	float Y2 = 0.0; //Point 3 of the ordinate
	float m = 0.0; //Slope of the line
	float Xm = 0.0; //Midpoint of the line in the abcissa
	float Ym = 0.0; //Midpoint of the line in the ordenate
	
	//Display of the phrase 1
	
	cout<<"Enter the point 1 of the abcissa: ";
	cin>>X1;
	cout<<"Enter the point 1 of the ordinate: ";
	cin>>Y1;
	cout<<"Enter the point 2 of the abcissa: ";
	cin>>X2;
	cout<<"Enter the point 2 of the ordinate: ";
	cin>>Y2;
	
	//Operations
	
	m=(Y2-Y1)/(X2-X1);
	Xm=(X1+X2)/2;
	Ym=(Y1+Y2)/2;
	
	//	Display of the phrase 2
	
	cout<<"\r\nThe slope of the line is: "<<m;
	cout<<"\r\nThe midpoint of the line in the abcissa is: "<<Xm;
	cout<<"\r\nThe midpoint of the line in the ordinate is: "<<Ym;
	
	return 0;
}
