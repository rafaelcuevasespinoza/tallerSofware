//Algorithm for calculate the volume of a tonel

/*

@rafaelcuevasespinoza@gmail.com
@Date 4/12/2021

*/

/***********************************************
 *  												INCLUDE
 ***********************************************/

#include <iostream>

using namespace std;


/***********************************************
 *  												MAIN
 ***********************************************/
 

//using function int main


int main(){
	
	//Declaration and Initialize of the variable
	
	float minordiameter = 0.0; //Minor diameter of the tonel
	float mayordiameter = 0.0; //Mayor diameter of the tonel 
	float a = 0.0; // a number that is part of V
	float l = 0.0; //Height of the tonel
	float Vc = 0.0; //Volume of the tonel in cubics centimeters
	float Vm = 0.0; //Volume of the tonel in cubics meters
	float Vl = 0.0; //Volume of the tonel in liters
	
	//Display phrase 1
	
	cout<<"Enter the minor diameter, in centimeters, of the tonel: ";
	cin>>minordiameter;
	cout<<"Enter the mayor diameter, in centimeters, of the tonel: ";
	cin>>mayordiameter;
	cout<<"Enter the height, in centimeters, of the tonel: ";
	cin>>l;
	
	//Operations
	
	a=minordiameter/2+(mayordiameter/2+minordiameter/2)*2/3;
	Vc=l*a*2*3.14;
	Vm=Vc/1000000;
	Vl=Vc/1000000000;
	
	// Display phrase 2
	
	cout<<"\r\nThe volume of the tonel in cubics centimeters is: "<<Vc;
	cout<<"\r\nThe volume of the tonel in cubics meters is: "<<Vm;
	cout<<"\r\nThe volume of the tonel in liters is: "<<Vl;
	
	return 0;
}
