/**
 * @file Exercise 20
 * @author Cuevas Espinoza Rafael Edu 
 * @brief Ingresar dos lados de un triangulo y el �ngulo que forman, 
   e imprima el  valor del tercer lado,  los otros dos �ngulos y 
   el �rea del tri�ngulo.  
 * @date 27.01.2022
 * 
 */
/*****************************************************************************INCLUDE**********************************************************************************************/

#include <iostream>
#include <math.h>  //BIBLIOTECA MATEMATICA 

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/


#define PI 	3.1416	//Constante PI



/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 
int main(){
	
	//Declaration y initialize
	double side_a     = 0.0 ;   //lado a del triangulo
	double side_b     = 0.0 ;   //lado b del triangulo
	double side_c     = 0.0 ;   //lado c del triangulo
	double angle_x    = 0.0 ;   //angulo x del triangulo
	double angle_y    = 0.0 ;   //angulo y del triangulo
	double angle_z    = 0.0 ;   //angulo z del triangulo
	double area       = 0.0 ;   //area del triangulo
	
	
	//Display phrase 1
	cout<<"Ingrese el primer lado del triangulo : ";
	cin>>side_a;
	
	cout<<"Ingrese el segundo lado del triangulo : ";
	cin>>side_b;
	
	cout<<"Ingrese el angulo que forman los dos lados mencionados : ";
	cin>>angle_x;
	
	//Operation
	side_c = sqrt( pow(side_a,2) + pow(side_b,2) - 2*side_a*side_b*cos(angle_x*PI/180) );
	area = side_a*side_b*sin(angle_x*PI/180)/2;
	angle_y = asin(side_b*sin(angle_x*PI/180)/side_c);
	angle_z = asin(side_a*sin(angle_x*PI/180)/side_c);
	
	//Display phrase 2
	cout<<"El tercer lado es:"<<side_c<<endl;                      //resultados del tercer lado
	cout<<"El area del triangulo es: "<<area<<endl;                //resultados del area del triamgulo
	cout<<"El segundo angulo del triangulo es: "<<angle_y<<endl;   //resultados del segundo angulo del triangulo
	cout<<"El tercer angulo del triangulo es: "<<angle_z<<endl;    //resultados del tercer angulo del triangulo

return 0;
}

