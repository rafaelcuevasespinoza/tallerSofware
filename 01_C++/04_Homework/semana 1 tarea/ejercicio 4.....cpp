/**
 * @file Exercise 4
 * @author Cuevas Espinoza Rafael Edu 
 * @brief Un departamento de climatolog�a ha realizado recientemente su conversi�n al sistema m�trico. Dise�ar un algoritmo para realizar las siguientes conversiones:
a. Leer la temperatura dada en la escala Celsius e imprimir en su equivalente Fahrenheit (la f�rmula de conversi�n es "F=9/5 �C+32").
b. Leer la cantidad de agua en pulgadas e imprimir su equivalente en mil�metros (25.5 mm = 1pulgada.

 * @date 27.01.2022
 * 
 */
/*****************************************************************************INCLUDE**********************************************************************************************/
#include <iostream>

using namespace std;
/*
Exercise 4
*/
//***************************************************************************FUNCTION DECLARATION**********************************************************************************
void Run();
void CollectData();
void Calculate();
void ShowResults();

/*******************************************************************MAIN*****************************************************************************************************************/

int main(){
	Run();
	return 0;
	}
//***********************************************************************GLOBAL VARIABLE*********************************************************************************************************
	double celsius;         //Grados celsius
	double fahrenheit;     //Grados fahrenheit
	double pulgadas;      //Pulgadas
	double mm;           //Milimetros
/********************************************************************FUNCTION DEFINITION**********************************************************************************************************/
void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//======================================================================================================================================================================================
void CollectData(){
	cout<<">Inserte la temperatura actual (en Celsius): ";
	cin>>celsius;
	cout<<"\n\r>Inserte la cantidad de agua (en pulgadas): ";
	cin>>pulgadas;
}
//===================================================================================================================================================================================
void Calculate() {
	fahrenheit = (celsius+32)*9 / (5);  //grados celcius a Fahrenheit
	mm = (pulgadas)*(25.5);            //pulgadas a milimetros
}
//=========================================================================================================================================================================================================================	
void ShowResults(){
	cout<<"La temperatura actual es: "<<fahrenheit <<"F"<<endl;         //resultado de la temperatura 
	cout<<"La cantidad de agua actual es de: "<<mm<<" mm"<<endl;       //resultado de la cantidad de agua
}

