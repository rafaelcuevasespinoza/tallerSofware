/**
 * @file Exercise 5
 * @author Cuevas Espinoza Rafael Edu 
 * @brief El costo de un autom�vil nuevo para un comprador es la suma total del costo del veh�culo, del porcentaje de la ganancia del vendedor y de los impuestos
  locales o estatales aplicables (sobre el precio de venta). Suponer una ganancia del vendedor del 12% en todas las unidades y un impuesto del 6% y dise�ar 
  un algoritmo para leer el costo total del autom�vil e imprimir el costo para el consumidor.
 * @date 27.01.2022
 * 
 */


/*****************************************************************************INCLUDE**********************************************************************************************/
#include <iostream>
using namespace std;
/*
Exercise 5
*/
//***************************************************************************FUNCTION DECLARATION**********************************************************************************

void Run();
void CollectData();                                              //funciones
void Calculate();
void ShowResults();

/*******************************************************************MAIN*****************************************************************************************************************/
int main() {
	
	Run();
	return 0;
	}
//***********************************************************************GLOBAL VARIABLE*********************************************************************************************************
	float ccomprador;                     //comprador
	float costototal;                     //costo total  
	float gana;                           //ganancia del vendedor
	float ganancia;                       //ganancia 
	float impuesto;                       //impuesto del 6%
/********************************************************************FUNCTION DEFINITION**********************************************************************************************************/
void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//======================================================================================================================================================================================
void CollectData(){

	cout << "Ingresar el costo total del vehiculo : " << endl;
	cin >> costototal;
}
//===================================================================================================================================================================================
void Calculate() {
	gana = costototal*0.12;
	impuesto = costototal*0.06;
	ccomprador = costototal+ganancia+impuesto;
}
//=========================================================================================================================================================================================================================	
void ShowResults(){

	cout << "El costo del vehiculo para el comprador es de S/." << ccomprador << endl;
	
}
