/**
 * @file Exercise 3
 * @author Cuevas Espinoza Rafael Edu 
 * @brief Queremos conocer los datos estad�sticos de una asignatura, por lo tanto, necesitamos un algoritmo que lea el n�mero de desaprobados, aprobados, notables y 
 sobresalientes de una asignatura, y nos devuelva:
a. El tanto por ciento de alumnos que han superado la asignatura.
b. El tanto por ciento de desaprobados, aprobados, notables y sobresalientes de la asignatura.

 * @date 27.01.2022
 * 
 */
/*****************************************************************************INCLUDE**********************************************************************************************/
#include <iostream>
using namespace std;
/*
Exercise 3
*/
//******************************************************************FUNCTION DECLARATION************************************************************************************************
void Run();
void CollectData();
void Calculate();
void ShowResults(); 
/*******************************************************************MAIN*****************************************************************************************************************/
int main() {
	Run();
	return 0;
	}
//******************************************************************************GLOBAL VARIABLE*************************************************************************************************	
	int aprobados;                                   //aprobados
	int desaprobados;                                //desaprobados
	int notables;                                    //notables
	float p_desaprobados;                            //porcentaje de desaprobados
	float p_notables;                                //porcentaje de notables
	float p_sobresalientes;                          //porcentaje de sobresalientes
	float sobresalientes;                            //sobresalientes
	float superan;                                   //superaron la materia
	float total;                                     //total entre aprobados y desaprobados
	
//********************************************************************FUNCTION DEFINITION*************************************************************************************************
void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//===================================================================================================================================================================================
void CollectData(){

	cout << "Ingresar cantidad de alumnos sobresalientes" << endl;
	cin >> sobresalientes;
	cout << "Ingresar cantidad de alumnos notables" << endl;
	cin >> notables;
	do {
		cout << "Ingresar cantidad de alumnos aprobados" << endl;
		cin >> aprobados;
	} while ((aprobados<notables && aprobados<sobresalientes));
	cout << "Ingresar cantidad de alumnos reprobados" << endl;
	cin >> desaprobados;
}
//======================================================================================================================================================================================================================
void Calculate(){
	total = (aprobados+desaprobados);
	superan = (aprobados/total)*100;
	p_notables = (notables/total)*100;
	p_sobresalientes = (sobresalientes/total)*100;
	p_desaprobados = (desaprobados/total)*100;
}
//=======================================================================================================================================================================================
void ShowResults(){
	cout << "El " << superan << "% superaron la materia" << endl;
	cout << "El " << p_desaprobados << "% reprobaron la materia" << endl;
	cout << "El " << p_notables << "% tienen notas notables" << endl;
	cout << "El " << p_sobresalientes << "% tienen notas sobresalientes" << endl;
}


