
//Algorithm for calculate the side a an side c of aa triangle by the law of sines

/*

@rafaelcuevasespinoza@gmail.com
@Date 4/12/2021

*/

/***********************************************
 *  												INCLUDE
 ***********************************************/


#include <iostream>
#include <math.h>

using namespace std;


/***********************************************
 *  												DEFINE
 ***********************************************/
 
 
 //Global variable
 
 int PI = 3.14;


/***********************************************
 *  												MAIN
 ***********************************************/


//Using the function int main

int main(){
	
	//Declaration and Initialization of the variables
	
    double alpha = 0.0; //Angle opposite of the side a
    double beta = 0.0; //Angle opposite of the side b
    double delta = 0.0; //Angle opposite of the side c
    double a = 0.0; //Side a of the triangle
    double b = 0.0; //Side b of the triangle
    double c = 0.0; //Side c of the triangle
    
    //Display of the phrase 1
    
    cout<<"Enter the angle opposite of the side a: ";
    cin>>alpha;
    cout<<"Enter the angle opposite of the side b: ";
    cin>>beta;
    cout<<"Enter the angle opposite of the side c: ";
	cin>>delta;
	cout<<"Enter the side c: ";
	cin>>c;
	
	//Operations
	
	a=(sin(alpha/180*PI)*c)/sin(delta/180*PI);
	b=(sin(beta/180*PI)*c)/sin(delta/180*PI);
	
	//Display of the phrase 2
	
	cout<<"\r\nThe oppsite side of the angle alpha is: "<<a;
	cout<<"\r\nThe oppisite side of the angle beta is: "<<b;
	
	
	return 0;
}
