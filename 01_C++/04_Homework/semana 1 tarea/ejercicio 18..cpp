/**
 * @file Exercise 18
 * @author Cuevas Espinoza Rafael Edu 
 * @brief Si coloca una escalera de 3 metros a un �ngulo de 85 grados al 
lado de un edificio, la altura en la cual la escalera toca el edificio
 se puede calcular como altura=3 * seno 85�. Calcule esta altura con una
 calculadora y luego escriba un programa en C que obtenga y visualice el
  valor de la altura.
 * @date 27.01.2022
 * 
 */
/*****************************************************************************INCLUDE**********************************************************************************************/
#include<iostream>
#include <math.h>  //BIBLIOTECA DE LAS FUNCIONES TRIGONOMETRICAS

using namespace std;
/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/


#define PI 	3.1416	//Constante PI



/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 

int main(){


	//Declaration
	int radian    = 0.0 ;        //radio
	double altura = 0.0 ;        //altura
	double result = 0.0 ;        //resultado

	
	//Display phrase 1
	cout<<"Escriba un numero para el angulo:";
	cin>>radian;

	//Operation
	result = sin(radian*PI/180);           //Se da la conversion del numero a radianes
	altura = 3*result;
	
	//Display phrase 2
	cout<<"La altura en la cual la escalera toca el edificio es:"<<altura;  //resultados de la altura
		
return 0;	
}


